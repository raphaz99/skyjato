﻿using Sky.SkyJato.Core.Domain.Washing;
using Sky.SkyJato.Core.Repositories.Interfaces;
using Sky.SkyJato.Dal.Context;
using Sky.SkyJato.Dal.Repositories.Base;

namespace Sky.SkyJato.Dal.Repositories
{
    public class ComplementRepository : BaseRepository<Complement>, IComplementRepositoy
    {
        public ComplementRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
