﻿using Sky.SkyJato.Core.Domain.Washing;
using Sky.SkyJato.Core.Repositories.Interfaces;
using Sky.SkyJato.Dal.Context;
using Sky.SkyJato.Dal.Repositories.Base;

namespace Sky.SkyJato.Dal.Repositories
{
    public class WashingRepository : BaseRepository<Washing>, IWashingRepository
    {
        public WashingRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
