﻿using Microsoft.EntityFrameworkCore;
using Sky.SkyJato.Core.Domain.Base;
using Sky.SkyJato.Core.Repositories.Base;
using Sky.SkyJato.Dal.Context;

namespace Sky.SkyJato.Dal.Repositories.Base
{
    public class BaseRepository<T> : IBaseRepository<T> where T : EntityBase
    {
        protected readonly AppDbContext Context;

        public BaseRepository(AppDbContext appDbContext)
        {
            Context = appDbContext;
        }
        public void Create(T entity)
        {
            Context.Add(entity);
        }
        public void Update(T entity)
        {
            Context.Update(entity);
        }
        public void Delete(T entity)
        {
            Context.Remove(entity);
        }

        public async Task<List<T>> GetAllAsync(CancellationToken cancellationToken)
        {
            return await Context.Set<T>().ToListAsync(cancellationToken);
        }

        public async Task<T> GetAsync(Guid id, CancellationToken cancellationToken)
        {
            return await Context.Set<T>().FirstOrDefaultAsync(x => x.Id == id, cancellationToken);
             
        }
    }
}
