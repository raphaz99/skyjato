﻿using Sky.SkyJato.Core.Repositories.Interfaces;
using Sky.SkyJato.Dal.Context;

namespace Sky.SkyJato.Dal.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;
        public UnitOfWork(AppDbContext context)
        {
            _context = context;
        }
        public async Task Commit(CancellationToken cancellationToken)
        {
            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}
