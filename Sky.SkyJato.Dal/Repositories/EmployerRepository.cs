﻿using Microsoft.EntityFrameworkCore;
using Sky.SkyJato.Core.Domain.Person;
using Sky.SkyJato.Core.Repositories.Interfaces;
using Sky.SkyJato.Dal.Context;
using Sky.SkyJato.Dal.Repositories.Base;

namespace Sky.SkyJato.Dal.Repositories
{
    public class EmployerRepository : BaseRepository<Employer>, IEmployerRepository
    {
        public EmployerRepository(AppDbContext appDbContext) : base(appDbContext) { }

        public async Task<Employer?> GetByCpfCnpjAsync(string cpf,CancellationToken cancellationToken)
        {
            return await Context.Employers.FirstOrDefaultAsync(x => x.CpfCnpj == cpf,cancellationToken);
        }
    }
}
