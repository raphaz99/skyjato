﻿using Microsoft.EntityFrameworkCore;
using Sky.SkyJato.Core.Domain.Person;
using Sky.SkyJato.Core.Domain.Washing;

namespace Sky.SkyJato.Dal.Context
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employer>().Property(x => x.Salary).HasPrecision(10, 2);
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Employer> Employers { get; set; }
        public DbSet<Washing> Washings { get; set; }
        public DbSet<Complement> Complements { get; set; }
    }
}
