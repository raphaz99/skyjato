﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Sky.SkyJato.Core.Repositories.Interfaces;
using Sky.SkyJato.Dal.Context;
using Sky.SkyJato.Dal.Repositories;

namespace Sky.SkyJato.Dal.Extensions
{
    public static class RepositoryServicesCollection
    {
        public static void AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IEmployerRepository, EmployerRepository>();
            services.AddScoped<IComplementRepositoy, ComplementRepository>();
            services.AddScoped<IWashingRepository, WashingRepository>();

        }
    }
}
