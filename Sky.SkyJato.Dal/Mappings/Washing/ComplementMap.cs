﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Sky.SkyJato.Core.Domain.Washing;

namespace Sky.SkyJato.Dal.Mappings.Washing
{
    public class ComplementMap : IEntityTypeConfiguration<Sky.SkyJato.Core.Domain.Washing.Complement>
    {
        public void Configure(EntityTypeBuilder<Complement> builder)
        {
            builder.ToTable("Complements");
            builder.HasKey(e => e.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd().UseIdentityColumn();

            builder.Property(x => x.Stock)
                .IsRequired()
                .HasColumnType("double");

            builder.Property(x => x.Value)
                .IsRequired()
                .HasColumnType("decimal(18,2)");
        }
    }
}
