﻿using Microsoft.EntityFrameworkCore;
namespace Sky.SkyJato.Dal.Mappings.Washing
{
    public class WashingMap : IEntityTypeConfiguration<Sky.SkyJato.Core.Domain.Washing.Washing>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Sky.SkyJato.Core.Domain.Washing.Washing> builder)
        {
            builder.ToTable("Washings");

            builder.HasKey(e => e.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd().UseIdentityColumn();

            builder.Property(x => x.LicensePlate)
                .IsRequired()
                .HasColumnType("varchar")
                .HasMaxLength(8);

            builder.Property(x => x.Date)
               .IsRequired()
               .HasColumnType("datetime");

            builder.Property(x => x.Observation)
            .HasColumnType("datetime")
            .HasMaxLength(400);

            builder.HasOne(w => w.Customer)
               .WithOne()
               .HasForeignKey<Sky.SkyJato.Core.Domain.Washing.Washing>(w => w.Customer)
               .IsRequired();

            builder.HasOne(w => w.Employer)
             .WithOne()
             .HasForeignKey<Sky.SkyJato.Core.Domain.Washing.Washing>(w => w.Employer)
             .IsRequired();

            builder.HasMany(w => w.Complements)
                .WithMany(e => e.Washings);
 
        }
    }
}
