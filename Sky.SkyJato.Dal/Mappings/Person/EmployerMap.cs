﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Sky.SkyJato.Core.Domain.Person;

namespace Sky.SkyJato.Dal.Mappings.Person
{
    public class EmployerMap : IEntityTypeConfiguration<Employer>
    {
        public void Configure(EntityTypeBuilder<Employer> builder)
        {
            builder.ToTable("Employers");

            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd().UseIdentityColumn();

            builder.Property(x => x.Name)
                .IsRequired()
                .HasColumnName("Name")
                .HasColumnType("varchar")
                .HasMaxLength(100);

            builder.Property(x => x.Salary)
                .HasColumnName("Salary")
                .HasColumnType("decimal")
                .HasPrecision(12, 10);

            builder.Property(x => x.Email)
              .IsRequired()
              .HasColumnName("Email")
              .HasColumnType("varchar")
              .HasMaxLength(50);

            builder.Property(x => x.FunctionDescription)
              .IsRequired()
              .HasColumnName("FunctionDescription")
              .HasColumnType("varchar")
              .HasMaxLength(300);

            builder.Property(x => x.CpfCnpj)
              .IsRequired()
              .HasColumnName("CpfCnpj")
              .HasColumnType("varchar")
              .HasMaxLength(13);


            builder.Property(x => x.CellPhone)
              .IsRequired()
              .HasColumnName("CellPhone")
              .HasColumnType("int");

            builder.Property(x => x.JoiningDate)
              .IsRequired()
              .HasColumnName("JoiningDate")
              .HasColumnType("datetime");

            builder.Property(x => x.ExitDate)
             .IsRequired()
             .HasColumnName("ExitDate")
             .HasColumnType("datetime");

            builder.HasIndex(x => x.Email, "IX_Employer_Email")
                .IsUnique();

            builder.HasIndex(x => x.CpfCnpj, "IX_Employer_CpfCnpj")
                .IsUnique();
        }
    }
}
