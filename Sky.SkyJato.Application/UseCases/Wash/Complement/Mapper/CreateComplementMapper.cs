﻿using AutoMapper;
using Sky.SkyJato.Application.UseCases.Wash.Complement.Dtos.Create;
using Sky.SkyJato.Core.Domain.Washing;

namespace Sky.SkyJato.Application.UseCases.Wash.Complement.Mapper
{
    public sealed class CreateComplementMapper : Profile
    {
        public CreateComplementMapper()
        {
            CreateMap<CreateComplementRequest, Sky.SkyJato.Core.Domain.Washing.Complement>();
            CreateMap<Sky.SkyJato.Core.Domain.Washing.Complement, CreateComplementRequest>();
        }
    }
}
