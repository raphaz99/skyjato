﻿using AutoMapper;
using MediatR;
using Sky.SkyJato.Application.UseCases.Wash.Complement.Dtos.Create;
using Sky.SkyJato.Application.UseCases.Wash.Washing.Dtos.Create;
using Sky.SkyJato.Core.Repositories.Interfaces;

namespace Sky.SkyJato.Application.UseCases.Wash.Complement.Create
{
    public class CreateComplementHandler : IRequestHandler<CreateComplementRequest, CreateComplementResponse>
    {
        private readonly IMapper _mapper;
        private readonly IComplementRepositoy _repository;
        private readonly IUnitOfWork _unitOfWork;

        public CreateComplementHandler(IMapper mapper, IComplementRepositoy repository, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _repository = repository;
            _unitOfWork = unitOfWork;

        }
        public async Task<CreateComplementResponse> Handle(CreateComplementRequest request, CancellationToken cancellationToken)
        {
            var complement = _mapper.Map<Sky.SkyJato.Core.Domain.Washing.Complement>(request);
            _repository.Create(complement);
            await _unitOfWork.Commit(cancellationToken);

            return _mapper.Map<CreateComplementResponse>(request);
        }
    }
}
