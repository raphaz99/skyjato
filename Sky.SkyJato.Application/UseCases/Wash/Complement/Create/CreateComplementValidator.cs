﻿using FluentValidation;
using Sky.SkyJato.Application.UseCases.Wash.Complement.Dtos.Create;

namespace Sky.SkyJato.Application.UseCases.Wash.Complement.Create
{
    public sealed class CreateComplementValidator : AbstractValidator<CreateComplementRequest>
    {

        public CreateComplementValidator()
        {
            RuleFor(x => x.Value).NotEmpty();
            RuleFor(x => x.Name).NotEmpty();

        }
    }
}
