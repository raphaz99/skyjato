﻿namespace Sky.SkyJato.Application.UseCases.Wash.Complement.Dtos.Create
{
    public sealed record CreateComplementResponse
    {
        public Guid Id { get; set; }
    }
}
