﻿using MediatR;

namespace Sky.SkyJato.Application.UseCases.Wash.Complement.Dtos.Create
{
    public sealed record CreateComplementRequest(string Name,
        decimal Value,
        double Stock) : IRequest<CreateComplementResponse>;
}
