﻿namespace Sky.SkyJato.Application.UseCases.Wash.Washing.Dtos.Create
{
    public sealed record CreateWashingResponse
    {

        public Guid Id { get; set; }
    }
}
