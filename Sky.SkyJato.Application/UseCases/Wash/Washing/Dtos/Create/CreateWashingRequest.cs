﻿using MediatR;
using Sky.SkyJato.Application.UseCases.Wash.Complement.Dtos.Create;
using Sky.SkyJato.Core.Domain.Person;

namespace Sky.SkyJato.Application.UseCases.Wash.Washing.Dtos.Create
{
    public sealed record CreateWashingRequest(DateTime Date,
        string LicensePlate,
        decimal Value,
        Core.Domain.Person.Employer Employer,
        List<CreateComplementRequest> Complements,
        string Observation,
        Customer Customer) : IRequest<CreateWashingResponse>;
}
