﻿using FluentValidation;
using Sky.SkyJato.Application.UseCases.Wash.Washing.Dtos.Create;

namespace Sky.SkyJato.Application.UseCases.Wash.Washing.Create
{
    public sealed class CreateWashingValidator : AbstractValidator<CreateWashingRequest>
    {
        public CreateWashingValidator()
        {

            RuleFor(x => x.Customer).NotEmpty();
            RuleFor(x => x.Employer).NotEmpty();
            RuleFor(x => x.LicensePlate).NotEmpty();

        }

    }
}
