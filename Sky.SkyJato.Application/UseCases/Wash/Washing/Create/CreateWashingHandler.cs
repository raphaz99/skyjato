﻿using AutoMapper;
using MediatR;
using Sky.SkyJato.Application.UseCases.Wash.Washing.Dtos.Create;

using Sky.SkyJato.Core.Repositories.Interfaces;

namespace Sky.SkyJato.Application.UseCases.Wash.Washing.Create
{
    public class CreateWashingHandler : IRequestHandler<CreateWashingRequest, CreateWashingResponse>
    {
        private readonly IMapper _mapper;
        private readonly IWashingRepository _repository;
        private readonly IUnitOfWork _unitOfWork;

        public CreateWashingHandler(IMapper mapper, IWashingRepository repository, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        public async Task<CreateWashingResponse> Handle(CreateWashingRequest request, CancellationToken cancellationToken)
        {
            var washing = _mapper.Map<Sky.SkyJato.Core.Domain.Washing.Washing>(request);
            _repository.Create(washing);
            await _unitOfWork.Commit(cancellationToken);

            return _mapper.Map<CreateWashingResponse>(request);
        }
    }    
}
