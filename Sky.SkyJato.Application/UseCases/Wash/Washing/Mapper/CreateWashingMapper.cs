﻿using AutoMapper;
using Sky.SkyJato.Application.UseCases.Wash.Washing.Dtos.Create;

namespace Sky.SkyJato.Application.UseCases.Wash.Washing.Mapper
{
    public sealed class CreateWashingMapper : Profile
    {

        public CreateWashingMapper()
        {
            CreateMap<CreateWashingRequest, Core.Domain.Washing.Washing>();
            CreateMap<Core.Domain.Washing.Washing, CreateWashingResponse>();
        }
    }
}
