﻿using AutoMapper;
using Sky.SkyJato.Application.UseCases.Employer.Dtos;

namespace Sky.SkyJato.Application.UseCases.Employer.Mapper
{
    public sealed class CreateEmployerMapper : Profile
    {
        public CreateEmployerMapper()
        {
            CreateMap<CreateEmployerRequest, Sky.SkyJato.Core.Domain.Person.Employer>();
            CreateMap<Sky.SkyJato.Core.Domain.Person.Employer, CreateEmployerResponse>();
        }
    }
}
