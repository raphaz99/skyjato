﻿namespace Sky.SkyJato.Application.UseCases.Employer.Dtos
{
    public sealed record CreateEmployerResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
