﻿using MediatR;

namespace Sky.SkyJato.Application.UseCases.Employer.Dtos
{
    public sealed record CreateEmployerRequest(string email,
        string name,         
        decimal salary,
        string cpfCnpj,
        string number,
        DateTime JoiningDate,
        DateTime? ingressDate,
        DateTime? exitDate,
        string functionDescription) : IRequest<CreateEmployerResponse>;

}
