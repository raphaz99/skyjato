﻿using FluentValidation;
using Sky.SkyJato.Application.UseCases.Employer.Dtos;

namespace Sky.SkyJato.Application.UseCases.Employer.Create
{
    public sealed class CreateEmployerValidator : AbstractValidator<CreateEmployerRequest>
    {
        public CreateEmployerValidator()
        {
            RuleFor(x => x.email).NotEmpty().MaximumLength(50).EmailAddress();
            RuleFor(x => x.cpfCnpj).NotEmpty().MaximumLength(15);
        }
    }
}
