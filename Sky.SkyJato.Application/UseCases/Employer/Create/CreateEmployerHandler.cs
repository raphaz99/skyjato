﻿using AutoMapper;
using MediatR;
using Sky.SkyJato.Application.UseCases.Employer.Dtos;
using Sky.SkyJato.Core.Repositories.Interfaces;

namespace Sky.SkyJato.Application.UseCases.Employer.Create
{
    public sealed class CreateEmployerHandler : IRequestHandler<CreateEmployerRequest, CreateEmployerResponse>
    {
        private readonly IMapper _mapper;
        private readonly IEmployerRepository _repository;
        private readonly IUnitOfWork _unitOfWork;
        public CreateEmployerHandler(IUnitOfWork unitOfWork, IEmployerRepository employerRepository, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _repository = employerRepository;
            _mapper = mapper;
        }
        public async Task<CreateEmployerResponse> Handle(CreateEmployerRequest request, CancellationToken cancellationToken)
        {
            var employer = _mapper.Map<SkyJato.Core.Domain.Person.Employer>(request);
            _repository.Create(employer);
            await _unitOfWork.Commit(cancellationToken);

            return _mapper.Map<CreateEmployerResponse>(employer);
        }
    }
}
