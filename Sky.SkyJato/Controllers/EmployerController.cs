﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Sky.SkyJato.Application.UseCases.Employer.Create;
using Sky.SkyJato.Application.UseCases.Employer.Dtos;

namespace Sky.SkyJato.Controllers
{
    [Route("api/[controller]/v1")]
    [ApiController]
    public class EmployerController : ControllerBase
    {
        private readonly IMediator _mediator;
        private CreateEmployerValidator _validator;

        public EmployerController(IMediator mediator)
        {
            _mediator = mediator;
            _validator = new CreateEmployerValidator();
        }

        [HttpPost]
        public async Task<ActionResult<CreateEmployerResponse>> Create(CreateEmployerRequest request, CancellationToken cancellationToken)
        {
            try
            {

                var result = await _validator.ValidateAsync(request);

                if (!result.IsValid)
                {
                    return BadRequest(result.Errors);
                }

                var response = await _mediator.Send(request, cancellationToken);

                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest($"Não foi possível criar o funcionário - {ex.Message} - {ex.StackTrace}");
            }
        }
    }
}
 