﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Sky.SkyJato.Application.UseCases.Employer.Create;
using Sky.SkyJato.Application.UseCases.Employer.Dtos;
using Sky.SkyJato.Application.UseCases.Wash.Washing.Create;
using Sky.SkyJato.Application.UseCases.Wash.Washing.Dtos.Create;

namespace Sky.SkyJato.Controllers
{
    [Route("api/[controller]/v1")]
    [ApiController]
    public class WashingController : ControllerBase
    {
        private readonly IMediator _mediator;
        private CreateWashingValidator _validator;


        public WashingController(IMediator mediator)
        {

            _mediator = mediator;
            _validator = new CreateWashingValidator();
        }

        [HttpPost]
        public async Task<ActionResult<CreateWashingResponse>> Create(CreateWashingRequest request, CancellationToken cancellationToken)
        {
            try
            {

                var result = await _validator.ValidateAsync(request);

                if (!result.IsValid)
                {
                    return BadRequest(result.Errors);
                }

                var response = await _mediator.Send(request, cancellationToken);

                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest($"Não foi possível criar a lavagem - {ex.Message} - {ex.StackTrace}");
            }
        }
    }
}
