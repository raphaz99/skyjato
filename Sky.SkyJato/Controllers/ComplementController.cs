﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Sky.SkyJato.Application.UseCases.Employer.Create;
using Sky.SkyJato.Application.UseCases.Employer.Dtos;
using Sky.SkyJato.Application.UseCases.Wash.Complement.Create;
using Sky.SkyJato.Application.UseCases.Wash.Complement.Dtos.Create;

namespace Sky.SkyJato.Controllers
{
    [ApiController]
    [Route("api/[controller]/v1")]
    public class ComplementController : ControllerBase
    {

        private readonly IMediator _mediator;
        private CreateComplementValidator _validator;

        public ComplementController(IMediator mediator)
        {
            _mediator = mediator;
            _validator = new CreateComplementValidator();
        }


        [HttpPost]
        public async Task<ActionResult<CreateComplementResponse>> Create(CreateComplementRequest request, CancellationToken cancellationToken)
        {
            try
            {

                var result = await _validator.ValidateAsync(request);

                if (!result.IsValid)
                {
                    return BadRequest(result.Errors);
                }

                var response = await _mediator.Send(request, cancellationToken);

                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest($"Não foi possível criar o complemento - {ex.Message} - {ex.StackTrace}");
            }
        }

    }
}
