﻿namespace Sky.SkyJato.Core.Domain.Base
{
    public abstract class EntityBase
    {
        public Guid Id { get; set; }
    }
}
