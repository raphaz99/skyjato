﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sky.SkyJato.Core.Domain.Enum
{
    public enum PersonEnum
    {
        Employee,
        Manager
    }
}
