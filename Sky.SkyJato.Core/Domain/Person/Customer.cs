﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace Sky.SkyJato.Core.Domain.Person
{
    public class Customer : Person
    {
        public int NumberOfWashes { get; set; }
    }
}
