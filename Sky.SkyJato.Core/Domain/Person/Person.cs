﻿using Sky.SkyJato.Core.Domain.Base;

namespace Sky.SkyJato.Core.Domain.Person
{

    public abstract class Person : EntityBase
    {
        public virtual string Name { get; set; }       
        public virtual string Email { get; set; }
        public virtual string CpfCnpj { get; set; }        
      
        public virtual string CellPhone { get; set; }

        public virtual string Address { get; set; }

    }
}
