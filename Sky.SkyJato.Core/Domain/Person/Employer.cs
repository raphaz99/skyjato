﻿namespace Sky.SkyJato.Core.Domain.Person
{
    public class Employer : Person
    {

        public virtual DateTime JoiningDate { get; set; } 
        public virtual DateTime ExitDate { get; set; }
        public virtual string FunctionDescription { get; private set; }

        public virtual Decimal Salary { get; set; }

    }

}


