﻿using Sky.SkyJato.Core.Domain.Base;
using Sky.SkyJato.Core.Domain.Person;

namespace Sky.SkyJato.Core.Domain.Washing
{
    public class Washing : EntityBase
    {
        public virtual DateTime Date { get; set; }

        public virtual string LicensePlate { get; set; }

        public virtual decimal Value { get; set; }

        public virtual Employer Employer { get; set; }

        public virtual ICollection<Complement> Complements { get; set; }

        public virtual string Observation { get; set; }

        public virtual Customer Customer { get; set; }

    }
}
