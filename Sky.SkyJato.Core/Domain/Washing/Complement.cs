﻿using Sky.SkyJato.Core.Domain.Base;

namespace Sky.SkyJato.Core.Domain.Washing
{
    public class Complement : EntityBase
    {

        public virtual string Name { get; set; }

        public virtual decimal Value { get; set; }

        public virtual double Stock { get; set; }

        public virtual ICollection<Washing> Washings { get; set; }

    }
}
