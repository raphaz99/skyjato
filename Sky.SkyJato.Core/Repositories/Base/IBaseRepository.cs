﻿using Sky.SkyJato.Core.Domain.Base;

namespace Sky.SkyJato.Core.Repositories.Base
{
    public interface IBaseRepository<T> where T : EntityBase
    {
        void Create(T entity);
        void Update(T entity);
        void Delete(T entity);

        Task<T> GetAsync(Guid id, CancellationToken cancellationToken);
        Task<List<T>> GetAllAsync(CancellationToken cancellationToken);
    }
}
