﻿using Sky.SkyJato.Core.Domain.Person;
using Sky.SkyJato.Core.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sky.SkyJato.Core.Repositories.Interfaces
{
    public interface IEmployerRepository : IBaseRepository<Employer>
    {
       Task<Employer?> GetByCpfCnpjAsync(string cpf,CancellationToken cancellationToken);
    }
}
