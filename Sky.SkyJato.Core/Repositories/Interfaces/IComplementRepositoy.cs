﻿using Sky.SkyJato.Core.Domain.Washing;
using Sky.SkyJato.Core.Repositories.Base;

namespace Sky.SkyJato.Core.Repositories.Interfaces
{
    public interface IComplementRepositoy : IBaseRepository<Complement>
    {
    }
}
